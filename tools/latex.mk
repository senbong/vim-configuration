.PHONY: clean

%.pdf: %.tex $(DEPENDS)
	-make clean
	rubber-info --check $<
	rubber -f --pdf -s $<

clean:
	rm -rf *.aux *.bbl *.blg *.log *.lot *.lof *.pdf *.toc *.snm *.out *.nav tags
