" LaTeX general make file
" http://blog.wuzzeb.org/posts/2013-04-05-latex-vim-plugins.html
setlocal errorformat=%f:%l:\ %m,%f:%l-%\\d%\\+:\ %m
if filereadable('Makefile')
    setlocal makeprg=make
else
    exec "setlocal makeprg=make\\ -f\\ ~/.vim/tools/latex.mk\\ " . 
                \substitute(bufname("%"),"tex","pdf", "")
endif

" Mapping for compiling and viewing pdf file
map <leader>v :!evince %:r.pdf &<cr>
map <leader>m :w<cr>:make<cr>
