" -------------------------------------------------------------------------
" VIM SETTINGS
" important ---------------------------------------------------------------
set nocompatible                                      "don't behave like Vi
    filetype off                                       "required for vundle
    " vundle --------------------------------------------------------------
    set rtp+=~/.vim/bundle/vundle/                     "required for vundle
    call vundle#rc()
    " add list of plugins
    Plugin 'gmarik/vundle'
    Plugin 'tpope/vim-fugitive'
    Plugin 'tpope/vim-unimpaired'
    Plugin 'tpope/vim-markdown'
    Plugin 'mattn/emmet-vim'
    Plugin 'Raimondi/delimitMate'
    Plugin 'scrooloose/syntastic'
    Plugin 'edsono/vim-matchit'
    Plugin 'tpope/vim-surround'
    Plugin 'vim-scripts/Align'
    Plugin 'scrooloose/nerdtree'
    Plugin 'SirVer/ultisnips'
    Plugin 'honza/vim-snippets'
    Plugin 'vim-scripts/LanguageTool'

    filetype plugin indent on
    syntax on                "put before colorscheme (cursorline highlight)
    colorscheme desert
" moving around, searching and patterns -----------------------------------
set incsearch                             "shows search matches as you type
set showmatch                                     "jump to matching bracket
set smartcase                                          "if caps, watch case
set ignorecase                               "if all lowercase, ignore case
" tags --------------------------------------------------------------------
" displaying text ---------------------------------------------------------
set wrap
set number 
set linebreak                                          "wraps between words
set scrolloff=1
" syntax, highlighting and spelling ---------------------------------------
set hlsearch                                     "highlights search results
set background=dark
set cursorline                               "highlight the cursor location
highlight CursorLine term=bold cterm=bold guibg=Grey40
" multiple windows --------------------------------------------------------
set hidden                               "allow to bg unsaved buffers, etc.
set winwidth=100                "min number of cols used for current window
set winminwidth=40                  "min number of cols used for any window
set winheight=5                "min number of lines used for current window
set winminheight=5                 "min number of lines used for any window
set winheight=999                                 " maximize current window
set laststatus=2                                   "always show status line
set statusline=%t\ \ line:%l/%L\ col:%c\ %p%%\ %y   "format the status line
" multiple tab pages ------------------------------------------------------
" terminal ----------------------------------------------------------------
" using the mouse ---------------------------------------------------------
" printing ----------------------------------------------------------------
" messages and info -------------------------------------------------------
set showcmd                                       "show normal etc commands
set ruler                                             "show cursor position
" selecting text ----------------------------------------------------------
" editing text ------------------------------------------------------------
set nrformats-=octal                  "0-prefixed numbers are still decimal
set backspace=indent,eol,start                          "proper backspacing
" tabs and indenting ------------------------------------------------------
set autoindent                           "auto set the indent of a new line
set smartindent                               "autoindent based on language
set expandtab                          "expand tab to spaces in insert mode
set smarttab                "a tab in an indent inserts 'shiftwidth' spaces
set shiftwidth=4             "number of spaces used for each step of indent
set softtabstop=4                     "number of spaces to insert for a tab
set tabstop=4                "number of spaces a tab in the text stands for
set shiftround                    "round > and < to multiples of shiftwidth
" folding -----------------------------------------------------------------
set foldmethod=marker
set foldmarker={{{,}}}
" diff mode ---------------------------------------------------------------
" mapping -----------------------------------------------------------------
set timeout                               "fixes slow O inserts (all three)
set timeoutlen=1000
set ttimeoutlen=100
" reading and writing file ------------------------------------------------
set autoread          "auto read a file when it was modified outside of vim
set autowrite             "auto write a file when leaving a modified buffer
set backup                                      "keep backup after o/w file
if &backupdir =~# '^\.,'
    let &backupdir = "/home/senbong/.vim/tmp/backup," . &backupdir
endif
" the swap file
" -------------------------------------------------------------------------
set swapfile
if &directory =~# '^\.,'
    let &directory = "/home/senbong/.vim/tmp/swap," . &directory
endif
" command line editing ----------------------------------------------------
set wildmenu
set wildmode=full
if v:version >= 703
    set undofile
    if &undodir =~# '^\.\%(,\|$\)'
        let &undodir = "/home/senbong/.vim/tmp/undo," . &undodir
    endif
endif
" executing external commands ---------------------------------------------
" running make and jumping to errors --------------------------------------
" language specific -------------------------------------------------------
" multi-byte characters ---------------------------------------------------
set shellslash
set encoding=utf-8              "must place before listchars to avoid error
set fileencoding=utf-8
" various -----------------------------------------------------------------
"set gdefault 
" gui setings -------------------------------------------------------------
if has("gui")
    set go-=T                                         "hide toolbar in mvim
    set guifont=inconsolata:h16
    set lines=65 columns=110
endif
" -------------------------------------------------------------------------
let mapleader=","
" mappings ----------------------------------------------------------------
nnoremap \ ,
nnoremap Y y$
nnoremap & :&&<cr>
nnoremap j gj
nnoremap k gk
nnoremap 0 g0
nnoremap $ g$
nnoremap <leader>ev :tabe $MYVIMRC<cr>
nnoremap <leader><leader> <c-^>
nnoremap <silent><c-l> :noh<cr>
noremap <leader>r mmHmt:%s/<c-v><cr>//ge<cr>'tzt'm
noremap <leader>l :set list! listchars=tab:→-,eol:¬<cr>
noremap <leader>s :set spell!<cr>
noremap <leader>c :w!<cr>:!aspell check %<cr>:e! %<cr>
" mappings for plugins  ---------------------------------------------------
nnoremap <c-n> :NERDTreeToggle<cr>
" abbreviations -----------------------------------------------------------
" autocommands ------------------------------------------------------------
autocmd BufReadPost * 
            \if line("'\"") > 0 && line("'\"") <= line("$") | 
            \exe "normal g'\"" | endif

augroup vimscript
    autocmd!
    autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END

augroup Python
    autocmd!
    autocmd Filetype python set nowrap
augroup END

augroup HTML
    autocmd!
    autocmd FileType html nnoremap <buffer> <leader>c I<!--<esc>A--><esc>
augroup END

augroup TEX
    autocmd!
    autocmd Filetype plaintex set filetype=tex
augroup END

augroup Markdown
    autocmd!
    autocmd BufNewFile,BufReadPost *.md set filetype=markdown
augroup END
" functions ---------------------------------------------------------------
function! InsertTabWrapper()
    " MULTIPURPOSE TAB KEY
    " Indent if we're at the beginning of a line. Else, do completion.
    " via https://github.com/garybernhardt/dotfiles/blob/master/.vimrc
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    else
        return "\<c-p>"
    endif
endfunction
inoremap <tab> <c-r>=InsertTabWrapper()<cr>
inoremap <s-tab> <c-n>

function! OpenUrlUnderCursor()
    let url=matchstr(getline("."), '[a-z]*:\/\/[^ >,;]*')
    if url != ""
        silent exec "!firefox '".url."'" | redraw! 
    endif
endfunction
map <leader>o :call OpenUrlUnderCursor()<CR>
" global variables for plugins --------------------------------------------
let g:NERDTreeWinPos         = 'right'
let g:NERDTreeShowHidden     = 1
let g:UltiSnipsExpandTrigger = '<c-j>'
let g:languagetool_jar       = '/home/senbong/bin/LanguageTool-2.5/languagetool-commandline.jar'
let g:loaded_syntastic_python_pylint_checker = 0
